import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { Input, Button, Icon } from "react-native-elements";
import { size } from "lodash";
import { useNavigation } from "@react-navigation/native";

import { validateEmail } from "../../utils/helpers";
import { registerUser } from "../../utils/actions";
import Loading from "../Loading";

const defaultFormValues = () => {
  return { email: "", password: "", confirm: "" };
};


export default function RegisterForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState(defaultFormValues());

  const [errorEmail, setErrorEmail] = useState("");
  const [errorPasword, setErrorPasword] = useState("");
  const [errorConfirm, setErrorConfirm] = useState("");
  const [loading, setLoading] = useState(false)

const navigation = useNavigation();


  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.nativeEvent.text });
  };

  const doRegisterUser = async () => {
    if (!validateData()) {
      return;
    }
    setLoading(true)
    const result = await registerUser(formData.email, formData.password);
    setLoading(false)
    if (!result.statusRsponse) {
        setErrorEmail(result.error)
        return
    }
    navigation.navigate("account")
  };

  const validateData = () => {
    setErrorConfirm("");
    setErrorEmail("");
    setErrorPasword("");
    let isValid = true;

    if (!validateEmail(formData.email)) {
      setErrorEmail("Debes ingresar un email valido.");
      isValid = false;
    }

    if (size(formData.password) < 6) {
      setErrorPasword("Debes ingres una contrasena mayor a 5 caracteres");
      isValid = false;
    }

    if (size(formData.confirm) < 6) {
      setErrorConfirm("Debes ingresar una confirmacion mayor de 5 caracteres");
      isValid = false;
    }

    if (formData.password !== formData.confirm) {
      setErrorPasword("La contraseña y la confirmacion no son iguales");
      setErrorConfirm("La contraseña y la confirmacion no son iguales");
      isValid = false;
    }
    return isValid;
  };
  return (
    <View style={styles.form}>
      <Input
        onChange={(e) => onChange(e, "email")}
        containerStyle={styles.input}
        placeholder="Ingresa tu email..."
        keyboardType="email-address"
        defaultValue={formData.email}
        errorMessage={errorEmail}
      />
      <Input
        containerStyle={styles.input}
        placeholder="Ingresa tu password..."
        password={true}
        onChange={(e) => onChange(e, "password")}
        secureTextEntry={!showPassword}
        defaultValue={formData.password}
        errorMessage={errorPasword}
        rightIcon={
          <Icon
            type="material-community"
            name={showPassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.icon}
            onPress={() => setShowPassword(!showPassword)}
          />
        }
      />
      <Input
        containerStyle={styles.input}
        placeholder="Confirma tu contraseña..."
        password={true}
        onChange={(e) => onChange(e, "confirm")}
        secureTextEntry={!showPassword}
        defaultValue={formData.confirm}
        errorMessage={errorConfirm}
        rightIcon={
          <Icon
            type="material-community"
            name={showPassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.icon}
            onPress={() => setShowPassword(!showPassword)}
          />
        }
      />
      <Button
        title="Registrar nuevo usuario"
        onPress={() => doRegisterUser()}
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
      />
      <Loading isVisible={loading} text="Creando cuenta..." />
    </View>
  );
}

const styles = StyleSheet.create({
  form: {
    marginTop: 30,
  },
  input: {
    width: "100%",
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
    alignSelf: "center",
  },
  btn: {
    backgroundColor: "#442484",
  },
  icon: {
    color: "#c1c1c1",
  },
});
